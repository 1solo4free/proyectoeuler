# El numero primo 10001
# Si listamos los primeros 6 numeros primos tenemos 2 3 4 7 11 y 13, por tanto podemos decir que el 13 es el 6to numero primo.
# Encuentre el numero primo que esta en la posicion 10001

import time
start = time.time()
# codigo

def es_primo(a):
    for i in range(2,int(a**0.5)+1):
        if a % i == 0:
            return False
    return True

posicion = 1
numero = 2

while True:
    if es_primo(numero):
        if posicion == 10001:
            print(numero)
            break
        posicion += 1
    numero += 1
    


# final
end = time.time()
tiempoTotal = end - start
print("TIEMPO DE EJECUCION: ", round(tiempoTotal,3),"segundos")