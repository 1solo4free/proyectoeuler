# 6 la suma de las potencias de 2 de los numeros del 1 al 10 es 385 o sea que 1 elevada a la 2 + 2 elevado a la dos mas 3 elevado a la 2 y asi
# sucesivamente hasta 10 elevado a la 2, la suma de todo esto es 385
# Y ahora si sumamos los primeros 10 numeros o sea 1 + 2 +3 + 4 y asi hasta 10 y el resultado de esta suma lo elevamos a la 2 obtenemos 3025
# y la diferecia entre estas dos sumas seria 3025-385=2640
# Halle esta misma difererncia pero para los numeros del 1 al 100

import time
start = time.time()
# codigo

limite = 100
suma = 0
sumaCuadrado = 0
cuadradoSuma = 0

for i in range(1, limite+1):
    suma = suma + i 
    cuadrado = i ** 2
    sumaCuadrado += cuadrado

cuadradoSuma = suma ** 2
resultado = cuadradoSuma - sumaCuadrado

print(resultado)

# final
end = time.time()
tiempoTotal = end - start
print("TIEMPO DE EJECUCION: ", round(tiempoTotal,3),"segundos")