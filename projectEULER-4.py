# un palindromo numerico es aquel que se lee el mismo numero del primero al ultimo que del ultimo al primero
# El palindromo numerico mayor que se puede formar con numeros de 2 digitos es 9009 que es la multiplicacion de 91 y 99
# encontrar el mayor palindromo que puede formarse con numeros de 3 digitos...
import time
start = time.time()
# codigo


a = 999
b = 999
mayor = 0

def es_palindromo(cadenaX):
    cadenaInv = ""
    for letra in cadenaX:
        cadenaInv = letra + cadenaInv
    if cadenaX == cadenaInv:
        return True
    else:
        return False

while b > 99:
    while a > 99:
        multiplico = a * b
        cadena = str(multiplico)
        if es_palindromo(cadena):
            if mayor < multiplico:
                mayor = multiplico
        a-=1
    b-=1
    a = 999

print (mayor)


# final
end = time.time()
tiempoTotal = end - start
print("TIEMPO DE EJECUCION: ", round(tiempoTotal,3),"segundos")