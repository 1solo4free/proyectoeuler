# 1 Múltiplos de 3 y 5 Si enumeramos todos los números naturales por debajo de 10 que son múltiplos de 3 o 5, 
# obtenemos 3, 5, 6 y 9. La suma de estos múltiplos es 23. Encuentra la suma de todos los múltiplos de 3 o 5 
# por debajo de 1000.

import time
start = time.time()  # estas 2 primeras lineas y las 3 ultimas son para determinar el tiempo de ejecucion.

# codigo
suma = 0
contador = 1
while contador < 1000:
    if (contador % 3 == 0) or (contador % 5 == 0):
        suma += contador
    contador += 1
    
print (suma)
# final del codigo

end = time.time()
tiempoTotal = end - start
print("TIEMPO DE EJECUCION: ", round(tiempoTotal,3),"segundos")

