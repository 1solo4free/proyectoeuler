# 5 2520 es el menor numero que puede ser dividido por todos los numeros entre 1 y 10 de 
# forma exacta. Cual seria el menor numero positivo que sea divisible de forma exacta por todos 
# los numeros entre 1 y 20

import time
start = time.time()
# codigo

def es_divisible(x):
    if x%20==0 and x%19==0 and x%18==0 and x%17==0 and x%16==0 and x%15==0 and x%14==0 and x%13==0 and x%12==0 and x%11==0 :
        return True
    else:
        return False

num = 20
contador = 1
booleano = True

while booleano:
    numero = num * contador
    if es_divisible(numero):
        print(numero)
        booleano = False
    contador += 1

# final
end = time.time()
tiempoTotal = end - start
print("TIEMPO DE EJECUCION: ", round(tiempoTotal,3),"segundos")