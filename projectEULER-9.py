# Tripleta Pitagorica
# Una tripleta pitagorica es un conjunto de 3 numeros naturales donde a<b<c, 
# para los cuales se cumple que a al cuadrado mas b al cuadrado es igual a c al cuadrado 
# por ejemplo 3 4 y 5 ya que 3 al cuadrado es 9 mas 4 al cuadrado es 16 y 9 mas 16 es 25 
# que es 5 al cuadrado
# Existe solo una tripleta que cumple que a+b+c=1000 ,encuentre el producto de esos 3 numeros

import time
start = time.time()
# codigo

a=1
booleano = True
while booleano:
    for b in range(1,1001):
        a2 = a**2
        b2 = b**2
        c = 1000-(a+b)
        c2 = c**2
        if (a2+b2)==c2:
            producto = a*b*c
            print(a,b,c)
            print(producto)
            booleano = False
    a+=1


# final
end = time.time()
tiempoTotal = end - start
print("TIEMPO DE EJECUCION: ", round(tiempoTotal,3),"segundos")