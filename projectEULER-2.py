#2 Números pares de Fibonacci Cada nuevo término de la secuencia de Fibonacci se genera sumando los dos 
# términos anteriores. Al comenzar con 1 y 2, los primeros 10 términos serán: 
# 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ... Al considerar los términos en la secuencia de Fibonacci 
# cuyos valores no exceden los cuatro millones, encuentre la suma de los términos pares.

import time
start = time.time()
# codigo

numAntes = 1
numAhora = 2
suma = 0
while numAhora < 4000000:
    if (numAhora % 2 == 0):
        suma += numAhora
    temp = numAhora
    numAhora = numAntes + numAhora
    numAntes = temp
print(suma)

# final
end = time.time()
tiempoTotal = end - start
print("TIEMPO DE EJECUCION: ", round(tiempoTotal,3),"segundos")